---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Manage your code **(FREE)**

Store your source files in a repository, create merge requests, and compile code hosted on GitLab.

- [Repositories](../user/project/repository/index.md)
- [Merge requests](../user/project/merge_requests/index.md)
- [Remote development](../user/project/remote_development/index.md)
